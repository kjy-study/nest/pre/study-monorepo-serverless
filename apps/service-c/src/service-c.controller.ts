import { Controller, Get } from '@nestjs/common';
import { ServiceCService } from './service-c.service';

@Controller()
export class ServiceCController {
  constructor(private readonly serviceCService: ServiceCService) {}

  @Get()
  getHello(): string {
    return this.serviceCService.getHello();
  }
}
